<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib prefix="springForm"
	uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Registration Form</title>

<!-- Import external CSS file -->
<link rel="stylesheet" href="css/custom.css">

<!-- To avoid favicon request -->
<link rel="icon" href="data:,">

</head>
<body>
	<div>
		<springForm:form action="register">

			<div>
				<springForm:label for="name" path="name">Name : </springForm:label>
				<springForm:input path="name" />
				<springForm:errors path="name" cssClass="error" />
			</div>
			<div>
				<springForm:label for="email" path="email">Email : </springForm:label>
				<springForm:input path="email" />
				<springForm:errors path="email" cssClass="error" />
			</div>
			<div>
				<springForm:label for="mobile" path="mobile">Mobile : </springForm:label>
				<springForm:input path="mobile" />
				<springForm:errors path="mobile" cssClass="error" />
			</div>
			<div>
				<input type="submit" value="Submit" />
			</div>
		</springForm:form>
	</div>
</body>
</html>