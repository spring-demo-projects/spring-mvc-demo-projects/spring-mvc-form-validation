package com.cdac.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.cdac.model.RegistrationFormModel;

@Controller
public class RegistrationController {

	@GetMapping(value = "/register", produces = "text/html")
	public String showRegistrationPage(Model model) {

		model.addAttribute("command", new RegistrationFormModel());

		return "registrationPage";
	}

	@PostMapping(value = "/register", produces = "text/html")
	public String register(@Valid @ModelAttribute("command") RegistrationFormModel registrationForm,
			BindingResult result) {
		if (result.hasErrors())
			return "registrationPage";
		return "success";
	}

}
